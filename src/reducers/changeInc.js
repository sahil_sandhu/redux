import { initialState } from "../actions/index";

const changeNumInc = (state = initialState, action) => {
    switch (action.type){
        case "INCREMENT" : return {
            ...state,
            count : state.count + 1
        }
        default: return state;
    }
}

export default changeNumInc;