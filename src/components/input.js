import {useSelector} from "react-redux";
import '../App.css'

function Input(){
    var currentState = useSelector((state)=>state.changeNum)
    return(
    <input className="display" name="quantity" type="text" value={currentState.count}/>
    );
}

export default Input;